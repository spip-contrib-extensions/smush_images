<?php
/**
 * Plugin Smush
 *
 * Auteur :
 * kent1 (http://www.kent1.info - kent1@arscenic.info)
 *
 * @package SPIP\Smushit\Fonctions
 */
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS['spip_matrice']['image_smush'] = 'inc/smush_image.php';
$GLOBALS['spip_matrice']['image_smush_debrayer'] = 'inc/smush_image.php';
$GLOBALS['spip_matrice']['image_smush_embrayer'] = 'inc/smush_image.php';
