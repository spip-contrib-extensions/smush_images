<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/smush_images.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'smush_description' => 'Le plugin Smush permet de réduire la taille des images d’un site au minimum en utilisant des logiciels spécifiques sur le serveur.',
	'smush_slogan' => 'Réduire ses images à peau de chagrin.'
);
