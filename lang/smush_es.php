<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/smush?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_copie_locale' => 'El archivo no ha podido ser copiado localmente (@src@).',
	'erreur_pas_image' => 'El archivo no parece ser una imagen de tipo png, jpg o gif (@src@).',

	// I
	'info_gain' => 'La ganancia es de @gain@ octetos siendo @gain_octets@ (@percent@%).',
	'info_gain_simple' => 'La ganancia es de @gain@ octetos (@percent@%).',
	'info_impossible_utilisation_locale' => 'No todos los softwares necesarios para una utilización local están presentes.',
	'info_logiciel_nok' => 'El software "@logiciel@" no está disponible',
	'info_logiciel_nok_tous' => 'El conjunto de softwares necesarios no son utilizables. No puede utilizar smush en local.',
	'info_logiciel_ok' => 'El softare "@logiciel@" parece utilizable',
	'info_logiciel_ok_tous' => 'El conjunto de softwares necesarios son utilizables. Puede utilizar smush en local.',
	'info_taille' => 'Tamaño: @taille@ (@octets@).',
	'info_titre_smush' => 'Smush',

	// L
	'label_eviter_traitement_auto' => 'Evitar el tratamiento automático de todas las imágenes generadas por SPIP',
	'label_url_test' => 'Url de su imagen a probar',

	// S
	'smush_conf_descriptif' => 'El plugin Smush permite reducir el tamaño de las imágenes de un sitio al mínimo utilizando los softwares específicos en el servidor. Esta página permite configurarlo.',

	// T
	'titre_test_logiciels' => 'Prueba de presencia de softwares',
	'titre_testeur_smush' => 'Probar en una imagen',
	'titre_version_originale' => 'Versión original',
	'titre_version_smushed' => 'Versión optimizada'
);
