<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-smush?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'smush_description' => 'The plugin Smush can reduce the image size to the minimum using specific softwares on the server.',
	'smush_slogan' => 'Reducing images to nearly nothing.'
);
