<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-smush?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'smush_description' => 'El plugin Smush permite reducir el tamaño de las imágenes de un sitio al mínimo utilizando softwares específicos en el servidor.',
	'smush_slogan' => 'Reducir sus imágenes al mínimo.'
);
