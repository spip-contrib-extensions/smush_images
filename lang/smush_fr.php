<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/smush_images.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_copie_locale' => 'Le fichier n’a pu être copié localement (@src@).',
	'erreur_pas_image' => 'Le fichier ne semble pas être une image de type png, jpg ou gif (@src@).',

	// I
	'info_gain' => 'Le gain est de @gain@ octets soit @gain_octets@ (@percent@%).',
	'info_gain_simple' => 'Le gain est de @gain@ octets (@percent@%).',
	'info_impossible_utilisation_locale' => 'Les logiciels nécessaires à une utilisation locale ne sont pas tous présents.',
	'info_logiciel_nok' => 'Le logiciel "@logiciel@" n’est pas disponible',
	'info_logiciel_nok_tous' => 'L’ensemble des logiciels nécessaires ne sont pas utilisables. Vous ne pouvez pas utiliser smush en local.',
	'info_logiciel_ok' => 'Le logiciel "@logiciel@" semble utilisable',
	'info_logiciel_ok_tous' => 'L’ensemble des logiciels nécessaires sont utilisables. Vous pouvez utiliser smush en local.',
	'info_taille' => 'Taille : @taille@ (@octets@).',
	'info_titre_smush' => 'Smush',

	// L
	'label_eviter_traitement_auto' => 'Éviter le traitement automatique de toutes les images générées par SPIP',
	'label_jpeg_qualite' => 'Qualité maximale de l’image JPEG (entre 0 et 100)',
	'label_url_test' => 'Url de votre image à tester',

	// S
	'smush_conf_descriptif' => 'Le plugin Smush permet de réduire la taille des images d’un site au minimum en utilisant des logiciels spécifiques sur le serveur. Cette page permet de le configurer.',

	// T
	'titre_test_logiciels' => 'Test de présence des logiciels',
	'titre_testeur_smush' => 'Tester sur une image',
	'titre_version_originale' => 'Version originale',
	'titre_version_smushed' => 'Version optimisée'
);
